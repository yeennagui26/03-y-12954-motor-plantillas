const express = require("express");
const hbs = require ("hbs");
const app = express();
require("dotenv").config();

//importamos archivos de rutas
const router = require("./routes/public");

app.use("/", router);

app.use(express.static(__dirname + "/public"));
app.set('view engine', 'hbs');
app.set('views', __dirname + "/views");

hbs.registerPartials(__dirname + "/views/partials");


app.listen(3000, () => {
    console.log("Servidor coriendo en el puerto 3000");
    console.log("http://localhost:3000/");
});
    
//console.log ("base de datos simulada", db);
//console.log (db.integrantes);
console.log(process.env.PORT);