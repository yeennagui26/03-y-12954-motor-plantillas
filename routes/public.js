const express = require("express");
const router = express.Router();
// Descomenta la siguiente línea si estás utilizando un archivo db/data.js
// const db = require("../db/data");
const { getALL } = require("../db/conexion");

require('dotenv').config({ path: '.env' });

router.get("/", async (request, response) => {
    try {
        // Obtener todos los datos de la tabla "Inicio" desde la base de datos //
        //const inicio = await getALL("SELECT * FROM Inicio");
       //console.log("DatosDeHome", inicio);
        //Obtener todos los integrantes desde la base de datos
        const Integrantes = await getALL("SELECT * FROM Integrantes");

        // Renderizar la página index con los datos obtenidos
        response.render("index", {
            //inicio: inicio,
            Integrantes: Integrantes,
            // Se traen los datos para el footer desde .env
            REPOSITORIO: process.env.REPOSITORIO,
            NOMBRE: process.env.NOMBRE,
            APELLIDO: process.env.APELLIDO,
            MATERIA: process.env.MATERIA
        });
    } catch (error) {
        // Manejo de errores
        console.error("Error al obtener los datos de inicio:", error);
        response.status(500).send("Error interno del servidor");
    }
});
router.get("/curso/", async (request, response) => {
    // Asegúrate de que getALL es una función que realiza la consulta a la base de datos
    const Integrantes = await getALL("SELECT * FROM Integrantes");
    response.render("curso", {
        Integrantes: Integrantes, // Utiliza la variable integrante en lugar de Integrantes
        REPOSITORIO: process.env.REPOSITORIO,
        NOMBRE: process.env.NOMBRE,
        APELLIDO: process.env.APELLIDO,
        MATERIA: process.env.MATERIA
    });
});

router.get("/word_cloud/", async (request, response) => {
    // Asegúrate de que getALL es una función que realiza la consulta a la base de datos
    const Integrantes = await getALL("SELECT * FROM Integrantes"); // Reemplaza db.integrantes con la lógica adecuada para obtener los integrantes de la base de datos
    response.render("word_cloud", {
        Integrantes: Integrantes,
        REPOSITORIO: process.env.REPOSITORIO,
        NOMBRE: process.env.NOMBRE,
        APELLIDO: process.env.APELLIDO,
        MATERIA: process.env.MATERIA
    });
});
//se modificaron los datos de los integrantes aqui...
router.get("/:matricula", async (request, response) => {
    const matricula = request.params.matricula;  
    const Integrantes = await getALL("SELECT * FROM Integrantes");                                  
    const integrante = await getALL('SELECT * FROM Integrantes WHERE matricula = \'' + matricula + '\'');
    const datos = await getALL('SELECT * FROM media WHERE matricula = \'' + matricula + '\'');
    if ( datos.length === 0) {
        response.status(404).render("error");
    } else {
        response.render("integrante", {
            data: datos,
            Integrantes: Integrantes,
            integrante: integrante, // Tomamos el primer integrante de la lista
            REPOSITORIO: process.env.REPOSITORIO,
            NOMBRE: process.env.NOMBRE,
            APELLIDO: process.env.APELLIDO,
            MATERIA: process.env.MATERIA
        });
    }
});


module.exports = router;