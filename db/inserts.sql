-- INSERT INTO la tabla de Integrantes
INSERT INTO Integrantes (matricula, nombre, apellido, url) VALUES
('Y26230', 'Zuanny', 'Ortiz', 'Y26230'),
('Y23715', 'Gabriela', 'Espinola', 'Y23715'),
('UG0045', 'Diego', 'Ramirez', 'UG0045'),
('Y12954', 'Yennifer', 'Aguilera', 'Y12954'),
('Y18433', 'Nicolas', 'Gimenez', 'Y18433');

-- INSERT INTO la tabla de TipoMedia
INSERT INTO TipoMedia (id_tipo_media, nombre) VALUES
(1, 'Imagen'),
(2, 'Youtube'),
(3, 'Dibujo');

-- INSERT INTO la tabla de Media
--nicolas media
-- INSERT INTO Media
INSERT INTO Media (id, titulo, nombre, url, matricula, id_tipo_media) VALUES 
(1, 'video Youtube', 'Youtube', 'https://www.youtube.com/embed/YZ8j6iO0ulw?si=3qmb7GQ0CxHN2C3Q', 'Y18433', 2),
(2, 'imagen elegida', 'imagen', '/images/mis_sueños.jpeg', 'Y18433', 1),
(3, 'dibujo hecho', 'dibujo', '/images/DibujoNico.png', 'Y18433', 3),

(4, 'video Youtube', 'Youtube', 'https://www.youtube.com/embed/210R0ozmLwg?si=bbjXLbzQS8fBaoTz', 'UG0045', 2),
(5, 'imagen elegida', 'imagen', '/images/imagen1Diego.jpeg', 'UG0045', 1),
(6, 'dibujo hecho', 'dibujo', '/images/Imagen2Diego.jpeg', 'UG0045', 3),

(7, 'video Youtube', 'Youtube', 'https://www.youtube.com/embed/yKNxeF4KMsY?si=_ipSNyBEBlw03PPN', 'Y12954', 2),
(8, 'imagen elegida', 'imagen', '/images/yeniimagen.jpg', 'Y12954', 1),
(9, 'dibujo hecho', 'dibujo', '/images/yenidibujo.jpg', 'Y12954', 3),

(10, 'video Youtube', 'Youtube', 'https://www.youtube.com/embed/Rk7gnFCeVAY', 'Y23715', 2),
(11, 'imagen elegida', 'imagen', '/images/Bob Esponja.jpeg', 'Y23715', 1),
(12, 'dibujo hecho', 'dibujo', '/images/bananamichi.png', 'Y23715', 3),

(13, 'video Youtube', 'Youtube', 'https://www.youtube.com/embed/Qes1RMK9a50?si=U5YTt8otRVQc1Ahk', 'Y26230', 2),
(14, 'imagen elegida', 'imagen', '/images/Extrovertida.jpg', 'Y26230', 1),
(15, 'dibujo hecho', 'dibujo', '/images/Dibujo_Zuanny.png', 'Y26230', 3);

