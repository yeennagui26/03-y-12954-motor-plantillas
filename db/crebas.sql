-- Tabla para almacenar los integrantes
CREATE TABLE IF NOT EXISTS Integrantes (
    matricula VARCHAR(20) PRIMARY KEY,
    nombre VARCHAR(45) NOT NULL,
    apellido VARCHAR(45) NOT NULL,  
    url VARCHAR(45),
    activo BOOLEAN DEFAULT 1
);

-- Tabla para almacenar los tipos de media
CREATE TABLE IF NOT EXISTS TipoMedia (
    id_tipo_media INT PRIMARY KEY,
    nombre VARCHAR(20),
    activo BOOLEAN DEFAULT 1
);

-- Tabla para almacenar los medios
CREATE TABLE IF NOT EXISTS Media (
    id INT PRIMARY KEY,
    titulo VARCHAR(45),
    nombre VARCHAR(45),
    url VARCHAR(45),
    matricula VARCHAR(20) NOT NULL,
    id_tipo_media INT NOT NULL,
    activo BOOLEAN DEFAULT 1,
    FOREIGN KEY (matricula) REFERENCES Integrantes(matricula),
    FOREIGN KEY (id_tipo_media) REFERENCES TipoMedia(id_tipo_media)
);
